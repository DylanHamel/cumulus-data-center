# Cumulus DC - Symmetrical IRB with VRF

###### Dylan Hamel - <dylan.hamel@protonmail.com>

> Use a ``virtualenv``.
>
> ```shell
> cd /path/to/cumulus-data-center
> python3 -m venv .
> source ./bin/activate
> ```
>
> Install ARA
> "ARA Records Ansible playbooks and makes them easier to understand and troubleshoot." 
> [link](https://ara.recordsansible.org/)
>
> ```shell
> pip install --upgrade pip
>  pip install -r requirements.txt 
> ```
>
> 

Backup is done with the following script : https://gitlab.com/DylanHamel/python-eveng-api 

Deploy the following network architecture with https://gitlab.com/DylanHamel/python-eveng-api script.
The configuration will not be deployed with this script. You have to run an Ansible playbook for this point.

```shell
» ./eveng-api.py --deploy=./topology.yml --force=True
```

![architecture](./images/architecture.png)





> There is a problem with MLAG.
>
> When MLAG is configured VNIs doesn't works

* Without MLAG

```shell
cumulus@leaf01:mgmt-vrf:~$ net show evpn vni
VNI        Type VxLAN IF              # MACs   # ARPs   # Remote VTEPs  Tenant VRF
100        L2   vni100                1        2        0               AMAZON
200        L2   vni200                1        4        0               GOOGLE
```

* With MLAG

```shell
cumulus@leaf05:mgmt-vrf:~$ net show evpn vni
VNI        Type VxLAN IF              # MACs   # ARPs   # Remote VTEPs  Tenant VRF
100        L2   vni100                0        0        0               AMAZON
200        L2   vni200                0        0        0               GOOGLE
280        L2   vni280                0        0        0               GOOGLE
```

New file ``topology_without_mlag.yml`` to remove MLAG.
There are only odd leaves (leaf01, leaf03, leaf05, exit01) and spine01 and spine02

![architecture_no_mlag](./images/architecture_no_mlag.png)



## IP Addresses (same in VRF)

| Devices Name | OOB - IP Adress | Loopback Adress | BGP - ASN |
| ------------ | --------------- | --------------- | --------- |
| Spine01      | 10.0.5.101/24   | 10.10.0.101/32  | 65100     |
| Spine02      | 10.0.5.102/24   | 10.10.0.102/32  | 65100     |
| Spine03      | 10.0.5.103/24   | 10.10.0.103/32  | 65100     |
| Leaf01       | 10.0.5.201/24   | 10.10.0.201/32  | 65212     |
| Leaf02       | 10.0.5.202/24   | 10.10.0.202/32  | 65212     |
| Leaf03       | 10.0.5.203/24   | 10.10.0.203/32  | 65234     |
| Leaf04       | 10.0.5.204/24   | 10.10.0.204/32  | 65234     |
| Leaf05       | 10.0.5.205/24   | 10.10.0.205/32  | 65256     |
| Leaf06       | 10.0.5.206/24   | 10.10.0.206/32  | 65256     |
| Exit01       | 10.0.5.151/24   | 10.10.0.151/32  | 65151     |
| Exit02       | 10.0.5.152/24   | 10.10.0.152/32  | 65151     |



## VLAN

| VLAN-ID   | VRF    | VLAN-Subnet    | Default Gateway |
| --------- | ------ | -------------- | --------------- |
| VLAN-1    | AMAZON | 10.1.1.0 /29   | 10.1.1.1        |
| VLAN-2    | GOOGLE | 10.2.2.0 /29   | 10.2.2.1        |
| VLAN-100  | AMAZON | 10.0.255.0 /24 | 10.0.255.1      |
| VLAN-190  | AMAZON | 10.1.255.0./24 | 10.0.255.1      |
| VLAN-200  | GOOGLE | 10.0.255.0 /24 | 10.0.255.1      |
| VLAN-280  | GOOGLE | 10.1.255.0 /24 | 10.1.255.1      |
| VLAN-290  | GOOGLE | 10.2.255.0 /24 | 10.2.255.1      |
| VLAN-1901 | AMAZON | ---            | --- (For L3VNI) |
| VLAN-2901 | GOOGLE | ---            | --- (For L3VNI) |



## VLAN Interfaces IP

| Device | VLAN-1   | VLAN-2   | VLAN-100    | VLAN-200    | VLAN-280    | VLAN-290    |
| ------ | -------- | -------- | ----------- | ----------- | ----------- | ----------- |
| Leaf01 |          |          | 10.0.255.11 | 10.0.255.11 | ---         | 10.2.255.11 |
| Leaf02 |          |          | 10.0.255.12 | 10.0.255.12 | ---         | 10.2.255.12 |
| Leaf03 |          |          | 10.0.255.13 | 10.0.255.13 | 10.1.255.13 | ---         |
| Leaf04 |          |          | 10.0.255.14 | 10.0.255.14 | 10.1.255.14 | ---         |
| Leaf05 |          |          | 10.0.255.15 | 10.0.255.15 | 10.1.255.15 | ---         |
| Leaf06 |          |          | 10.0.255.16 | 10.0.255.16 | 10.1.255.16 | ---         |
| Exit01 | 10.1.1.5 | 10.2.2.5 | ---         | ---         | ---         | ---         |
| Exit02 | 10.1.1.6 | 10.2.2.6 | ---         | ---         | ---         | ---         |
|        |          |          |             |             |             |             |



## VXLAN

| VXLAN-ID (VNI) | Type (L2VNI / L3VNI) | VLAN-Bridge | VRF    | Comments                     |
| -------------- | -------------------- | ----------- | ------ | ---------------------------- |
| VNI-100        | L2VNI                | VLAN-100    | AMAZON | On all leaves                |
| VNI-200        | L2VNI                | VLAN-200    | GOOGLE | On all leaves                |
| VNI-280        | L2VNI                | VLAN-280    | GOOGLE | On Leaf03 + 04 / Leaf05 + 06 |
| VNI-290        | ---                  | ---         | ---    | VLAN Only on Leaf01 and 02   |
|                |                      |             |        |                              |
|                |                      |             |        |                              |
| VNI-409001     | L3VNI                | VLAN-2901   | GOOGLE |                              |
| VNI-409002     | L3VNI                | VLAN-2902   | AMAZON |                              |
|                |                      |             |        |                              |
|                |                      |             |        |                              |



## Configuration

#### L2VNI

```shell
net add vlan 1901 vrf AMAZON

net add vxlan vni409002 vxlan id 409002
net add vxlan vni409002 vxlan local-tunnelip 10.10.0.205
net add vxlan vni409002 bridge access 1901
net add vxlan vni409002 bridge learning off
net add vxlan vni409002 mtu 9216

net commit
```

```shell
cumulus@leaf01:mgmt-vrf:~$ net show evpn vni
VNI        Type VxLAN IF              # MACs   # ARPs   # Remote VTEPs  Tenant VRF
100        L2   vni100                4        8        2               AMAZON
200        L2   vni200                4        8        2               GOOGLE
409002     L2   vni409002             1        1        0               AMAZON
409001     L3   vni409001             2        2        n/a             GOOGLE
```



#### L3VNI

The following command will change a L2VNI to a L3VNI

```shell
net add vrf AMAZON vni 409002
net commit
```

```shell
cumulus@leaf01:mgmt-vrf:~$ net show evpn vni
VNI        Type VxLAN IF              # MACs   # ARPs   # Remote VTEPs  Tenant VRF
100        L2   vni100                4        8        2               AMAZON
200        L2   vni200                4        8        2               GOOGLE
409002     L3   vni409002             0        0        n/a             AMAZON
409001     L3   vni409001             2        2        n/a             GOOGLE
```



### BGP routing in Overlay

L3VNI is up but ``leaf03`` has no information about "how join the network 10.1.255.0/24". This network is only connected on the ``leaf01``. For resolve that, you need to exchange IPv4 routes via EVPN (Type 5).

```shell
net add bgp vrf AMAZON autonomous-system 65156
net add bgp vrf AMAZON l2vpn evpn advertise ipv4 unicast
net add bgp vrf AMAZON ipv4 unicast redistribute connected
# ON EXIT - advertise default ip gateway to go on Internet
net add bgp vrf AMAZON ipv4 unicast redistribute static
net add bgp vrf AMAZON l2vpn evpn advertise-default-gw
```

```shell
router bgp 65112
  # Underlay ------------------------------------------------------------
  bgp router-id 10.10.0.201
  bgp bestpath as-path multipath-relax
  neighbor swp1 interface remote-as external
  neighbor swp2 interface remote-as external

  address-family ipv4 unicast
    redistribute connected route-map LOOPBACK_ONLY
    
	# Overlay ------------------------------------------------------------
  address-family l2vpn evpn
    neighbor swp1 activate
    neighbor swp2 activate
    advertise-all-vni
    advertise-default-gw
    advertise ipv4 unicast

router bgp 65112 vrf AMAZON

  address-family ipv4 unicast
    redistribute connected

  address-family l2vpn evpn
    advertise ipv4 unicast

```

Leaf03 and Leaf05 will receive informations about network ``10.1.255.0/24``

```shell
cumulus@leaf03:mgmt-vrf:~$ net show bgp vrf AMAZON ipv4 unicast
BGP table version is 4, local router ID is 10.0.255.13
Status codes: s suppressed, d damped, h history, * valid, > best, = multipath,
              i internal, r RIB-failure, S Stale, R Removed
Origin codes: i - IGP, e - EGP, ? - incomplete

   Network          Next Hop            Metric LocPrf Weight Path
*  10.0.255.0/24    10.10.0.201                            0 65100 65112 ?
*                   10.10.0.205                            0 65100 65156 ?
*                   10.10.0.201                            0 65100 65112 ?
*                   10.10.0.205                            0 65100 65156 ?
*>                  0.0.0.0                  							 0 32768 ?
*  10.0.255.11/32   10.10.0.201                            0 65100 65112 i
*>                  10.10.0.201                            0 65100 65112 i
*  10.0.255.15/32   10.10.0.205                            0 65100 65156 i
*>                  10.10.0.205                            0 65100 65156 i
*  10.1.255.0/24    10.10.0.201                            0 65100 65112 ?
*>                  10.10.0.201                            0 65100 65112 ?

Displayed  4 routes and 11 total paths
```



## Issues

On ``leaf01`` (no MLAG) ping ``leaf06`` (no MLAG) => It works

```shell
64 bytes from 10.0.255.16: icmp_seq=13 ttl=64 time=2.40 ms
64 bytes from 10.0.255.16: icmp_seq=14 ttl=64 time=1.92 ms
64 bytes from 10.0.255.16: icmp_seq=15 ttl=64 time=3.69 ms
```

Configure ``MLAG`` between ``leaf05`` and ``leaf06``

```shell
# Leaf05
net add clag peer sys-mac 44:38:39:FF:00:56 \
	interface swp8-9 \
	primary \
	backup-ip 192.168.0.12
	
# Leaf06
net add clag peer sys-mac 44:38:39:FF:00:56 \
	interface swp8-9 \
	secondary \
	backup-ip 192.168.0.11
```

Cumulus Linux MLAG with BGP.

```shell
auto peerlink
iface peerlink
    bond-slaves swp8 swp9

auto peerlink.4094
iface peerlink.4094
    address 169.254.1.2/30
    clagd-backup-ip 192.168.0.11
    clagd-peer-ip 169.254.1.1
    clagd-priority 2000
    clagd-sys-mac 44:38:39:FF:00:56
```

Ping doesn't work from ``leaf01``.

```shell
64 bytes from 10.0.255.16: icmp_seq=133 ttl=64 time=2.54 ms
64 bytes from 10.0.255.16: icmp_seq=134 ttl=64 time=1.60 ms
64 bytes from 10.0.255.16: icmp_seq=135 ttl=64 time=1.88 ms
64 bytes from 10.0.255.16: icmp_seq=136 ttl=64 time=2.40 ms
64 bytes from 10.0.255.16: icmp_seq=137 ttl=64 time=1.92 ms
64 bytes from 10.0.255.16: icmp_seq=138 ttl=64 time=3.69 ms
64 bytes from 10.0.255.16: icmp_seq=139 ttl=64 time=2.28 ms
64 bytes from 10.0.255.16: icmp_seq=140 ttl=64 time=1.67 ms

^C
--- 10.0.255.16 ping statistics ---
791 packets transmitted, 140 received, 82% packet loss, time 790445ms
rtt min/avg/max/mdev = 1.219/2.433/17.343/2.051 ms
```

#### Remove MLAG configuration

```shell
net del clag port bond peerlink
net del clag peer
```

```shell
sudo ifreload -a
```

Ping from ``leaf01`` work.

```shell
64 bytes from 10.0.255.15: icmp_seq=583 ttl=64 time=2.34 ms
64 bytes from 10.0.255.15: icmp_seq=584 ttl=64 time=1.88 ms
64 bytes from 10.0.255.15: icmp_seq=585 ttl=64 time=2.82 ms
64 bytes from 10.0.255.15: icmp_seq=586 ttl=64 time=7.36 ms
64 bytes from 10.0.255.15: icmp_seq=587 ttl=64 time=1.91 ms
64 bytes from 10.0.255.15: icmp_seq=588 ttl=64 time=2.00 ms
64 bytes from 10.0.255.15: icmp_seq=589 ttl=64 time=2.50 ms
64 bytes from 10.0.255.15: icmp_seq=590 ttl=64 time=1.63 ms
```

#### Restart CLAG deamon

```shell
cumulus@leaf05:mgmt-vrf:~$ net show clag
cumulus@leaf05:mgmt-vrf:~$
cumulus@leaf05:mgmt-vrf:~$ sudo systemctl restart clagd
cumulus@leaf05:mgmt-vrf:~$ net show clag
The peer is not alive
     Our Priority, ID, and Role: 1000 50:00:00:08:00:08 secondary
          Peer Interface and IP: peerlink.4094 169.254.1.2
                      Backup IP: 192.168.0.12 (inactive)
                     System MAC: 44:38:39:ff:00:56

CLAG Interfaces
Our Interface      Peer Interface    CLAG Id  Conflicts  Proto-Down Reason
----------------   ----------------  -------  ---------- -----------------
          vni100   -                 -        -          init,vxlan-single,no-anycast-ip
          vni200   -                 -       -           init,vxlan-single,no-anycast-ip
```

Execute the same command on ``leaf06``

```shell
cumulus@leaf06:mgmt-vrf:~$ net show clag
The peer is alive
    Peer Priority, ID, and Role: 1000 50:00:00:08:00:08 primary
     Our Priority, ID, and Role: 2000 50:00:00:09:00:08 secondary
          Peer Interface and IP: peerlink.4094 169.254.1.1
                      Backup IP: 192.168.0.11 (inactive)
                     System MAC: 44:38:39:ff:00:56

CLAG Interfaces
Our Interface      Peer Interface     CLAG Id   Conflicts     Proto-Down Reason
----------------   ----------------   -------   -----------   -----------------
          vni100   -                  -         -             vxlan-single,no-anycast-ip
          vni280   -                  -         -             vxlan-single,no-anycast-ip
          vni200   -                  -         -             vxlan-single,no-anycast-ip
```

