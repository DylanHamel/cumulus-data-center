#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Production"
__copyright__ = "Copyright 2019"
__license__ = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#

try:
    import json
except ImportError as importError:
    print("Error import [main-batfish] json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from pybatfish.client.commands import *
    from pybatfish.question.question import load_questions
    from pybatfish.datamodel.flow import (HeaderConstraints, PathConstraints)
    from pybatfish.question import bfq
except ImportError as importError:
    print("Error import [main-batfish] batfish")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
NETWORK_NAME = "cumulus-virtual-network"
SNAPSHOT_NAME = "cumulus-virtual-network"
SNAPSHOT_PATH = "./cumulus-network/"

######################################################
#
# Functions
#
def myprint():
    print ("###########################################")

######################################################
#
# MAIN Functions
#
def main():

    load_questions()
    bf_set_network(NETWORK_NAME)
    bf_init_snapshot(SNAPSHOT_PATH, name=SNAPSHOT_NAME, overwrite=True)
    
    print(bfq.bgpSessionStatus().answer().frame())

    myprint()

    print(bfq.routes().answer().frame())

# -----------------------------------------------------------------------------------------------------------------------------
#
#
if __name__ == "__main__":
    main()
